#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:2cb3c7afdd7d8c69844bc7c43c1e9d09ce471af8; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:2a70fb1bd6772b6fafa3045ca55ef36e69728480 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:2cb3c7afdd7d8c69844bc7c43c1e9d09ce471af8 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
